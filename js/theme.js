(function() {

	"use strict";

	var Core = {

		initialized: false,

		initialize: function() {

			if (this.initialized) return;
			this.initialized = true;

			this.build();
			this.events();

		},

		build: function() {

			this.navMenu();
			this.navMobMenu();
			this.owlCarousel();

		},
		
		navMenu: function() {

			// Responsive Menu Events
			var addActiveClass = false;

			$("#mainMenu li.dropdown > a, #mainMenu li.dropdown-submenu > a").on("click", function(e) {

				if($(window).width() > 979) return;

				e.preventDefault();

				addActiveClass = $(this).parent().hasClass("resp-active");

				$("#mainMenu").find(".resp-active").removeClass("resp-active");

				if(!addActiveClass) {
					$(this).parents("li").addClass("resp-active");
				}

				return;

			});

			// Submenu Check Visible Space
			$("#mainMenu li.dropdown-submenu").hover(function() {

				if($(window).width() < 767) return;

				var subMenu = $(this).find("ul.dropdown-menu");

				if(!subMenu.get(0)) return;

				var screenWidth = $(window).width(),
					subMenuOffset = subMenu.offset(),
					subMenuWidth = subMenu.width(),
					subMenuParentWidth = subMenu.parents("ul.dropdown-menu").width(),
					subMenuPosRight = subMenu.offset().left + subMenu.width();

				if(subMenuPosRight > screenWidth) {
					subMenu.css("margin-left", "-" + (subMenuParentWidth + subMenuWidth + 10) + "px");
				} else {
					subMenu.css("margin-left", 0);
				}

			});

			// Mega Menu
			$(document).on("click", ".mega-menu .dropdown-menu", function(e) {
				e.stopPropagation()
			});

		},
		
		navMobMenu: function() {

			// Responsive Menu Events
			var addActiveClass = false;

			$("#mainMenu2 li.dropdown > a, #mainMenu2 li.dropdown-submenu > a").on("click", function(e) {

				if($(window).width() > 979) return;

				e.preventDefault();

				addActiveClass = $(this).parent().hasClass("resp-active");

				$("#mainMenu2").find(".resp-active").removeClass("resp-active");

				if(!addActiveClass) {
					$(this).parents("li").addClass("resp-active");
				}

				return;

			});

			// Submenu Check Visible Space
			$("#mainMenu2 li.dropdown-submenu").hover(function() {

				if($(window).width() < 767) return;

				var subMenu = $(this).find("ul.dropdown-menu");

				if(!subMenu.get(0)) return;

				var screenWidth = $(window).width(),
					subMenuOffset = subMenu.offset(),
					subMenuWidth = subMenu.width(),
					subMenuParentWidth = subMenu.parents("ul.dropdown-menu").width(),
					subMenuPosRight = subMenu.offset().left + subMenu.width();

				if(subMenuPosRight > screenWidth) {
					subMenu.css("margin-left", "-" + (subMenuParentWidth + subMenuWidth + 10) + "px");
				} else {
					subMenu.css("margin-left", 0);
				}

			});

			// Mega Menu
			$(document).on("click", ".mega-menu .dropdown-menu", function(e) {
				e.stopPropagation()
			});

		},

		owlCarousel: function(options) {

			var total = $("div.owl-carousel:not(.manual)").length,
				count = 0;

			$("div.owl-carousel:not(.manual)").each(function() {

				var slider = $(this);

				var defaults = {
					 // Most important owl features
					items : 5,
					itemsCustom : false,
					itemsDesktop : [1199,4],
					itemsDesktopSmall : [980,3],
					itemsTablet: [768,2],
					itemsTabletSmall: false,
					itemsMobile : [479,1],
					singleItem : true,
					itemsScaleUp : false,

					//Basic Speeds
					slideSpeed : 200,
					paginationSpeed : 800,
					rewindSpeed : 1000,

					//Autoplay
					autoPlay : false,
					stopOnHover : false,

					// Navigation
					navigation : false,
					navigationText : ["<i class=\"icon icon-chevron-left\"></i>","<i class=\"icon icon-chevron-right\"></i>"],
					rewindNav : true,
					scrollPerPage : false,

					//Pagination
					pagination : true,
					paginationNumbers: false,

					// Responsive
					responsive: true,
					responsiveRefreshRate : 200,
					responsiveBaseWidth: window,

					// CSS Styles
					baseClass : "owl-carousel",
					theme : "owl-theme",

					//Lazy load
					lazyLoad : false,
					lazyFollow : true,
					lazyEffect : "fade",

					//Auto height
					autoHeight : false,

					//JSON
					jsonPath : false,
					jsonSuccess : false,

					//Mouse Events
					dragBeforeAnimFinish : true,
					mouseDrag : true,
					touchDrag : true,

					//Transitions
					transitionStyle : false,

					// Other
					addClassActive : false,

					//Callbacks
					beforeUpdate : false,
					afterUpdate : false,
					beforeInit: false,
					afterInit: false,
					beforeMove: false,
					afterMove: false,
					afterAction: false,
					startDragging : false,
					afterLazyLoad : false
				}

				var config = $.extend({}, defaults, options, slider.data("plugin-options"));

				// Initialize Slider
				slider.owlCarousel(config).addClass("owl-carousel-init");

			});

		},

		masonry: function() {

			var $container;

			$(".masonry-list").each(function() {

				var $container = $(this);

				$container.waitForImages(function() {

					$container.masonry({
						itemSelector: '.masonry-item'
					});

					$container.addClass("init");

				});

			});

		}

	};

	Core.initialize();

	$(window).load(function () {

		Core.masonry();

	});
	

})();