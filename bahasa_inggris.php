<?php

/*MENU BAR*/
$lang_menu_home ="Home";
$lang_menu_about ="About";
$lang_menu_proser ="Product & Service";
$lang_menu_news ="News";
$lang_menu_video ="Video";
$lang_menu_location ="Location";
$lang_menu_language ="Language";


/*CAROUSEL UTAMA*/

/*KONTEN ABOUT*/
$lang_title_about = "ABOUT FLEXLIVE";
$lang_subtitle_about = "What is Flexlive";
$lang_content_about = "Flexlive is a mobile platform application that contains various kinds of entertainment content and also information about the country of Indonesia. Created and developed by PT. Zaflan Apta Media";
/*KONTEN PRODUCT AND SERVICE*/
$lang_title_proser = "PRODUCT & SERVICE";
$lang_subtitle_proser = "Know Our Product & Service";

$lang_title_contproser1 ="Live Streaming";
$lang_contentproser1 ="Live streaming is a term that refers to content that is broadcast live through internet media. Live streaming can be in the form of video and audio. Currently the live streaming service can be live streaming TV and streaming radio";
$lang_title_contproser2 ="Live Chat";
$lang_contentproser2 ="LiveChat is an online customer service software with live support, help desk software, and web analytics capabilities. It was first launched in 2002 and is currently developed and offered in a SaaS (software as a service) business model by LiveChat Software.";
$lang_title_contproser3 ="E-Commerce";
$lang_contentproser3 ="Ecommerce, also known as electronic commerce or internet commerce, refers to the buying and selling of goods or services using the internet, and the transfer of money and data to execute these transactions. Ecommerce is often used to refer to the sale of physical products online, but it can also describe any kind of commercial transaction that is facilitated through the internet.";
/*KONTEN NEWS*/

/*KONTEN VIDEO*/

/*KONTEN LOCATION*/

/*KONTEN NAVIGASI*/

/*KONTEN FOOTER*/
$lang_footer_title_about = "ABOUT";
$lang_footer_content_about = "Flex live is a mobile platform application that contains various kinds of entertainment masters and also information about the country of Indonesia";
$lang_footer_navigation = "NAVIGATION";
$lang_footer_getupdates = "GET UPDATES";


?>