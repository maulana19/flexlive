<?php

/*MENU BAR*/
$lang_menu_home ="Beranda";
$lang_menu_about ="Tentang Kami";
$lang_menu_proser ="Produk & Layanan";
$lang_menu_news ="Berita";
$lang_menu_video ="Video";
$lang_menu_location ="Lokasi";
$lang_menu_language ="Bahasa";

/*CAROUSEL UTAMA*/

/*KONTEN ABOUT*/
$lang_title_about = "TENTANG FLEXLIVE";
$lang_subtitle_about = "Apa itu Flexlive";
$lang_content_about = "Flexlive adalah aplikasi platform mobile yang berisi berbagai macam konten hiburan dan juga informasi seputar negara indonesia. Dibuat dan dikembangkan oleh PT. Zaflan Apta Media";


/*KONTEN PRODUCT AND SERVICE*/
$lang_title_proser = "PRODUK DAN LAYANAN";
$lang_subtitle_proser = "Ketahui Produk dan Layanan Kami";


$lang_title_contproser1 ="Siaran Langsung";
$lang_contentproser1 ="Live streaming adalah istilah yang mengacu pada konten yang disiarkan langsung melalui media internet. Live streming dapat berupa video dan audio. Saat ini layanan live streaming bisa berupa live streaming tv dan radio streaming";
$lang_title_contproser2 ="Percakapan Langsung";
$lang_contentproser2 ="LiveChat adalah perangkat lunak layanan pelanggan online dengan dukungan langsung, perangkat lunak help desk, dan kemampuan analisis web. Ini pertama kali diluncurkan pada tahun 2002 dan saat ini sedang dikembangkan dan ditawarkan dalam model bisnis SaaS (perangkat lunak sebagai layanan) oleh LiveChat Software.";
$lang_title_contproser3 ="Jual Beli Online";
$lang_contentproser3 ="E-commerce, juga dikenal sebagai perdagangan elektronik atau perdagangan internet, mengacu pada pembelian dan penjualan barang atau jasa menggunakan internet, dan transfer uang dan data untuk melakukan transaksi ini. Ecommerce sering digunakan untuk merujuk pada penjualan produk fisik secara online, tetapi juga dapat menggambarkan segala jenis transaksi komersial yang difasilitasi melalui internet.";
/*KONTEN NEWS*/
$lang_title_news = "BERITA";
$lang_subtitle_news = "Dapatkan berita terbaru seputar negara Indonesia";
$lang_content_news1 = "Flexlive adalah aplikasi platform mobile yang berisi berbagai macam konten hiburan dan juga informasi seputar negara indonesia. Dibuat dan dikembangkan oleh PT. Zaflan Apta Media";

/*KONTEN VIDEO*/

/*KONTEN LOCATION*/

/*KONTEN NAVIGASI*/

/*KONTEN FOOTER*/
$lang_footer_title_about = "TENTANG";
$lang_footer_content_about = "Flexlive adalah aplikasi platform mobile yang berisi berbagai macam konten hiburan dan juga informasi seputar negara indonesia. Dibuat dan dikembangkan oleh PT. Zaflan Apta Media";
$lang_footer_navigation = "NAVIGASI";
$lang_footer_getupdates = "DAPATKAN PEMBAHARUAN";


?>