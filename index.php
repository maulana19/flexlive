﻿<?php
// memulai sesi
session_start();
// bahasa default website
$default_lang = 'bahasa_inggris';

// jika user merubah bahasa
if($_GET['lang']) {
  // ubah bahasa sesuai keinginan user
  $_SESSION['lang'] = $_GET['lang'];
  // kembalikan ke halaman index.php
  header("Location: index.php");
}

// jika tidak ada bahasa terdeteksi
if(!$_SESSION['lang']) {
  // atur bahasa ke bahasa default
  $_SESSION['lang'] = $default_lang;
}

// masukan file bahasa yang sedang aktif
include $_SESSION['lang'] . '.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Flex Live</title>
		<meta name="description" content="Travel" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Alegreya|Alegreya+SC|Oswald:400,300" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Raleway|Roboto" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/bootstrap.min.css" />
		<link rel="stylesheet" href="css/fonts/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="css/theme.css" />
		<link rel="stylesheet" href="css/theme-elements.css" />
		<link rel="stylesheet" href="css/theme-responsive.css" /> 
		<link rel="stylesheet" href="css/docs.theme.min.css">
		<link rel="stylesheet" href="css/owl.carousel.min.css">
		<link rel="stylesheet" href="css/owl.theme.default.min.css">
		<link rel="stylesheet" href="css/style-gue.css">
		<link rel="stylesheet" href="css/hover.css">
		<link rel="stylesheet" href="css/animated.css">
		<link rel="stylesheet" type="text/js" src="js/bootstrap.js">
		<link rel="stylesheet" type="text/js" src="js/bootstrap.min.js">
		<link rel="shorcut icon" href="img/flexlive-mini.png">
		
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script type="text/javascript" src="js/custom.js"></script>
		<noscript>
			<link rel="stylesheet" type="text/css" href="css/styleNoJS.css" />
		</noscript>

		<link href="assets/css/rvslider.min.css" rel="stylesheet">
	</head>
	<body id="mainMenu">
		
		<div class="body">
			<header id="header">
				<div class="container">
					<h1 class="logo float-center" style="position: fixed;">
						<a href="index.php" style="text-decoration:none;font-family: 'Righteous', cursive;color:#fff;font-size:0.6em;"><img src="img/flexlive-mini.png" class="rounded" width="78" height="64"></a>
					</h1>
					<div class="nav-main-collapse navbar-collapse collapse" id="nav-main-collapse">
						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu">
								<li class="hvr-fade">
									<a href="#about">
										<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><?php echo $lang_menu_about;  ?></p>
										</div>
									</a>
								</li>
								<li class="hvr-fade">
									<a href="#proser">
										<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><?php echo $lang_menu_proser;  ?></p>
										</div>
									</a>
								</li>
								<li class="hvr-fade">
									<a href="#news">
										<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><?php echo $lang_menu_news;  ?></p>
										</div>
									</a>
								</li>
								<li class="hvr-fade">
									<a href="#pideo">
										<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><?php echo $lang_menu_video;  ?></p>
										</div>
									</a>
								</li>
								<li class="hvr-fade">
									<a href="#location">
											<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><?php echo $lang_menu_location;  ?></p>
											</div>
									</a>
								</li>
								<!-- <li class="full-menu-join">
									<a href="signup.html">
										<div style="display:inline-block">
											<p style="margin:0;color:#fff;font-weight:bold;font-family"><img src="img/joinnow.png" width="100"></p>
										</div>
									</a>
								</li> -->
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #fff"><img src="img/ML.png" style="width: 20px;height: 20px;"></a>
									<ul class="dropdown-menu">
										<li><a href="?lang=bahasa_inggris"><img src="img/UK.png" id="bendera">English</a></li>
										<li><a href="?lang=bahasa_indonesia"><img src="img/ID.png" id="bendera">Indonesia</a></li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
				</div>
				
				<!-- <div style="background-color: #999999;width: 100%;position: fixed;top: 0px;left: 0px;" data-toggle="collapse" data-parent="#accordion"> -->
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-parent="#accordion" href="#collapse2" style="position: fixed;background-color: #999999;">
						<i class="icon icon-user"></i>
					</button>
					
					<button class="btn btn-responsive-nav btn-inverse" style="background-color: #999999;left:0;margin-left:15px;position: fixed;" data-toggle="collapse" onclick="openNav()">
						<i class="icon icon-bars"></i>
					</button>
				<!-- </div> -->
				
				<div id="mySidenav" class="sidenav">
					<div style="height:150px;background-image:url('img/flexmenu.png');background-size: 250px 150px;">
						<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>						
					</div>
					<div style="width:250px"><a href="#mainMenu" class="hvr-fade"><i class="icon icon-home" style="margin-right:15px"></i>Home</a></div>
					<div style="width:250px"><a href="#about" class="hvr-fade"><i class="icon icon-question" style="margin-right:15px"></i>About</a></div>
					<div style="width:250px"><a href="#proser" class="hvr-fade"><i class="icon icon-shopping-cart" style="margin-right:15px"></i>Product & Service</a></div>
					<div style="width:250px"><a href="#news" class="hvr-fade"><i class="icon icon-calendar" style="margin-right:15px"></i>News</a></div>
					<div style="width:250px"><a href="#pideo" class="hvr-fade"><i class="icon icon-film" style="margin-right:15px"></i>Video</a></div>
					<div style="width:250px"><a href="#location" class="hvr-fade"><i class="icon icon-map-marker" style="margin-right:15px"></i>Location</a></div>
					
					
					<div class="side-menu-join" align="center"><a href="#" style="padding:8px"><img src="img/joinnow.png" width="100"></a></div>
				</div>
			</header>
			
			<div class="main2 home-slider">
				<div class="container demo-1">
					<div id="slider" class="sl-slider-wrapper">
						<div class="sl-slider">
						
							<div class="sl-slide bg-1" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
								<div class="sl-slide-inner" style="background: url(img/image2.jpg) no-repeat center center fixed;width: 100vw;height:100vh;background-size:cover;">
									<div style="background:rgba(0,0,0,0.7);width:100vw;height:100vh">
										<h2>Enjoys 1 Million++ Videos</h2>
										<blockquote><p>With varieties category that you can choose and watch it with your family or beloved ones.</p><cite>Dame Jane Morris Goodall</cite></blockquote>
									</div>
								</div>
							</div>
						
							<div class="sl-slide bg-1" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
								<div class="sl-slide-inner" style="background: url(img/gym.jpg) no-repeat center center fixed;width: 100vw;height:100vh;background-size:cover;">
									<div style="background:rgba(0,0,0,0.7);width:100vw;height:100vh">
										<h2>Best place for getting start a healthy life.</h2>
										<blockquote><p>You have just dined, and however scrupulously the slaughterhouse is concealed in the graceful distance of miles, there is complicity.</p><cite>Ralph Waldo Emerson</cite></blockquote>
									</div>
								</div>
							</div>
							
							<div class="sl-slide bg-1" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
								<div class="sl-slide-inner" style="background: url(img/gym3.jpg) no-repeat center center fixed;width: 100vw;height:100vh;background-size:cover;">
									<div style="background:rgba(0,0,0,0.7);width:100vw;height:100vh">
										<h2>Everything your body needs was here</h2>
										<blockquote><p>The human body has no more need for cows' milk than it does for dogs' milk, horses' milk, or giraffes' milk.</p><cite>Michael Klaper M.D.</cite></blockquote>
									</div>
								</div>
							</div>
						</div>
						
						<nav id="nav-arrows" class="nav-arrows">
							<span class="nav-arrow-prev">Previous</span>
							<span class="nav-arrow-next">Next</span>
						</nav>

						<nav id="nav-dots" class="nav-dots">
							<span class="nav-dot-current"></span>
							<span></span>
							<span></span>
						</nav>
					</div>
				</div>
			</div>
		</div>

		<section id="about" class="section-position" style="background-color: #E6E7E8">
			<div class="container-fluid">
				<div class="row">
					<div class="large-12 columns">
						<h3 class="animated bounce" align="center" style="color:#111;font-weight:bold;margin:90px 0px 0px"><?php echo $lang_title_about; ?></h3>
						<p align="center" style="color:#D60B27;font-weight:bold;margin:10px 0px 20px;"><?php echo $lang_subtitle_about; ?></p>

						<!-- <div class="container" style="margin-top: 20px">
							<div class="about-border"> -->
								<!-- <div class="col-md-6 meta"> -->
									<!-- <div class="owl-carousel-promo owl-carousel owl-theme-about">
										<img src="img/2.jpg" height="300px">
										<img src="img/3.jpg" height="300px">
										<img src="img/4.png" height="300px">
									</div> -->
									<!-- <img src="img/1.jpg" style="width:600px; height:360px;" class="image img-thumbnail" align="center">
									<div class="overlay">
										<div class="text"><img src="img/flexlive-mini.png"></div>
									</div> -->
								<!-- </div> -->
								<!-- <div class="col-md-6 description">
									<h4 align="left" style="color:#111;font-weight:bold;">Flex Live</h4>
									<hr>
									<p align="left" style="color: #000;line-height: 30px;font-size: 15px;"><?php echo $lang_content_about; ?></p>
								</div>
							</div>
						</div> -->

						<div class="row" style="margin-top: 15px;">
							<div class="col-md-9" style="margin-left: 12%;">
								<div class="ab-card">
								    <div class="column-about">
								    	<div class="owl-carousel-promo owl-carousel owl-theme-about">
											<img src="img/1.jpg" height="200px">
											<img src="img/2.jpg" height="200px">
											<img src="img/3.jpg" height="200px">
										</div>
								    </div>
								    	
									<div class="ab-description">
								      <h4 align="left" style="color:#111;font-weight:bold;">Flex Live</h4>
										<hr>
										<p align="left" style="color: #000;line-height: 30px;font-size: 15px;"><?php echo $lang_content_about; ?></p>
									</div>
								</div>
							</div>
						</div>

						
					</div>
				</div>
			</div>
		</section>

		<section id="proser" style="background-color: #222222">
			<div class="container-fluid">
				<div class="row">
					<div class="large-12 columns">
						<h3 align="center" style="color: #fff;font-weight: bold;margin: 80px 0px 0px"><?php echo $lang_title_proser?></h3>
						<p align="center" style="color: #2AA6F0;font-weight: bold;margin: 10px 0px 20px"><?php echo $lang_subtitle_proser?></p>
					</div>

					<!-- AWAL BARIS PRODUK-->
					<div class="row" style="margin-top: 150px">

						<div class="col-sm-12 col-md-4" align="center"><!-- KOLOM IKON PERTAMA -->
							<div class="product-border zoom-effect-border" style="margin-bottom: 35px;">
							<img src="img/proser1.png" class="aikon zoom-effect">
							<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser1;?></h3>
							<p align="justify" style="color: #2AA6F0">Watch Your Favorite Channel Anywhere</p>
							</div>
						</div><!-- AKHIR KOLOM IKON TERAKHIR -->

						<div class="col-sm-12 col-md-4" align="center"><!-- KOLOM IKON KEDUA -->
							<div class="product-border zoom-effect-border" style="margin-bottom: 25px;">
							<img src="img/proser2.png" class="aikon zoom-effect">
							<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser2;?></h3>
							<p align="justify" style="color: #2AA6F0">Chat With Us Directly</p>
							</div>
						</div><!-- AKHIR KOLOM KETIGA -->
						
						<div class="col-sm-12 col-md-4" align="center"><!-- KOLOM IKON TERAKHIR -->
							<div class="product-border zoom-effect-border" style="margin-bottom: 25px;">
							<img src="img/proser3.png" class="aikon zoom-effect">
							<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser3;?></h3>
							<p align="justify" style="color: #2AA6F0">Buy What You Need Online Here</p>
							</div>
						</div><!-- AKHIR KOLOM IKON TERAKHIR -->

					</div><!-- AKHIR BARIS PRODUK-->


				</div>

				<!-- AWAL BARIS 5 Column-->

				<div class="large-12 columns">
					<h3 align="center" style="color: #fff;font-weight: bold;margin: 80px 0px 0px"><?php echo $lang_title_proser?></h3>
					<p align="center" style="color: #2AA6F0;font-weight: bold;margin: 10px 0px 20px"><?php echo $lang_subtitle_proser?></p>
				</div>

				<div class="col-md-12 mg-left-32-service mg-top-146-service">

					<div class="col-md-2" align="center"><!-- KOLOM IKON PERTAMA -->
						<div class="product-border-5 zoom-effect-border" style="margin-bottom: 35px;">
						<img src="img/proser1.png" class="aikon zoom-effect">
						<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser1;?></h3>
						<p align="justify" style="color: #2AA6F0">Watch Your Favorite Channel Anywhere</p>
						</div>
					</div><!-- AKHIR KOLOM IKON TERAKHIR -->

					<div class="col-md-2 mg-left-24-service" align="center"><!-- KOLOM IKON KEDUA -->
						<div class="product-border-5 zoom-effect-border" style="margin-bottom: 25px;">
						<img src="img/proser2.png" class="aikon zoom-effect">
						<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser2;?></h3>
						<p align="justify" style="color: #2AA6F0">Chat With Us Directly</p>
						</div>
					</div><!-- AKHIR KOLOM KETIGA -->
					
					<div class="col-md-2 mg-left-24-service" align="center"><!-- KOLOM IKON PERTAMA -->
						<div class="product-border-5 zoom-effect-border" style="margin-bottom: 35px;">
						<img src="img/proser1.png" class="aikon zoom-effect">
						<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser1;?></h3>
						<p align="justify" style="color: #2AA6F0">Watch Your Favorite Channel Anywhere</p>
						</div>
					</div><!-- AKHIR KOLOM IKON TERAKHIR -->

					<div class="col-md-2 mg-left-24-service" align="center"><!-- KOLOM IKON KEDUA -->
						<div class="product-border-5 zoom-effect-border" style="margin-bottom: 25px;">
						<img src="img/proser2.png" class="aikon zoom-effect">
						<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser2;?></h3>
						<p align="justify" style="color: #2AA6F0">Chat With Us Directly</p>
						</div>
					</div><!-- AKHIR KOLOM KETIGA -->

					<div class="col-md-2 mg-left-24-service" align="center"><!-- KOLOM IKON PERTAMA -->
						<div class="product-border-5 zoom-effect-border" style="margin-bottom: 35px;">
						<img src="img/proser1.png" class="aikon zoom-effect">
						<h3 align="center" style="margin-top: 20px;color: #EEEEEE;"><?php echo $lang_title_contproser1;?></h3>
						<p align="justify" style="color: #2AA6F0">Watch Your Favorite Channel Anywhere</p>
						</div>
					</div><!-- AKHIR KOLOM IKON TERAKHIR -->

				</div><!-- AKHIR BARIS 5 Column-->


			</div>
		</section>
			
			<section id="news" class="section-position" style="background-color: #E6E7E8">
				<div class="container-fluid">
					<div class="row">
						<div class="large-12 columns">
							<h3 align="center" style="color:#111;font-weight:bold;margin:80px 0px 0px">Hot News</h3>
							<p align="center" style="color:#D60B27;font-weight:bold;margin:10px 0px 20px;">Get updated news and promotion of Flex Live here.</p>
						</div>	
					</div>

					<div class="row" style="margin-top: 15px;">
						<div class="col-md-6">
							<div class="blog-card">
							    <div class="meta">
							      <img src="img/proser1.png" class="img-news">
							    </div>
							    <div class="description">
							      <h1 style="font-weight: bold;">Ini Judul Berita</h1>
							      <p style="color: #222"> Ini adalah isi Berita yang mengabarkan suatu kabar,kejadian,tragedi,dan lain-lain yang kebenaran nya masih belum bisa dipastikan oleh pihak yang terkait</p>
							      <p class="read-more">
							        <a data-toggle="modal" data-target="#myModal">Read More</a>
							      </p>
							    </div>
							 </div>
						 </div>

						 <div class="col-md-6">
							 <div class="blog-card">
							    <div class="meta">
							      <img src="img/proser1.png" class="img-news">
							    </div>
							    <div class="description">
							      <h1 style="font-weight: bold;">Ini Judul Berita 33</h1>
							      <p style="color: #222"> Ini adalah isi Berita yang mengabarkan suatu kabar,kejadian,tragedi,dan lain-lain yang kebenaran nya masih belum bisa dipastikan oleh pihak yang terkait. </p>
							      <p class="read-more">
							        <a data-toggle="modal" data-target="#myModal2">Read More</a>
							      </p>
							    </div>
							 </div>
						 </div>

					</div>

					<div class="row" style="margin-top: 15px;">
						<div class="col-md-6">
							<div class="blog-card">
							    <div class="meta">
							      <img src="img/proser1.png" class="img-news">
							    </div>
							    <div class="description">
							      <h1 style="font-weight: bold;">Ini Judul Berita</h1>
							      <p style="color: #222"> Ini adalah isi Berita yang mengabarkan suatu kabar,kejadian,tragedi,dan lain-lain yang kebenaran nya masih belum bisa dipastikan oleh pihak yang terkait</p>
							      <p class="read-more">
							        <a data-toggle="modal" data-target="#myModal3">Read More</a>
							      </p>
							    </div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="blog-card">
							    <div class="meta">
							      <img src="img/proser1.png" class="img-news">
							    </div>
							    <div class="description">
							      <h1 style="font-weight: bold;">Ini Judul Berita</h1>
							      <p style="color: #222"> Ini adalah isi Berita yang mengabarkan suatu kabar,kejadian,tragedi,dan lain-lain yang kebenaran nya masih belum bisa dipastikan oleh pihak yang terkait</p>
							      <p class="read-more">
							        <a data-toggle="modal" data-target="#myModal4">Read More</a>
							      </p>
							    </div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</section>

			<br><br>

			<section id="demos" class="section-position" style="background-color:#222222">
				<div id="pideo">
					<div class="row">
						<div class="large-12 columns">
							<h3 align="center" style="color:#fff;font-weight:bold;margin:80px 0px 0px;">Our Video</h3>
							<p align="center" style="color:#D60B27;font-weight:bold;margin:10px 0px 20px;">Watch Our Collection Videos</p>
							<div class="owl-carousel-tips owl-carousel owl-theme">
								<div class="item-video" data-merge="3">
									<a class="owl-video" href="https://www.youtube.com/watch?v=04kpfNu8qoU&t=107s"></a> 
								</div>
								<div class="item-video" data-merge="3">
									<a class="owl-video" href="https://www.youtube.com/watch?v=FN2PtGIFESY"></a> 
								</div>
								<div class="item-video" data-merge="3">
									<a class="owl-video" href="https://www.youtube.com/watch?v=xXV-1zURKuc"></a> 
								</div>
								<div class="item-video" data-merge="3">
									<a class="owl-video" href="https://www.youtube.com/watch?v=XkygBnOGZfg"></a> 
								</div>
								<div class="item-video" data-merge="3">
									<a class="owl-video" href="https://www.youtube.com/watch?v=3ywLqOJTWW8"></a> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>		
							

			<div id="location" class="section-position" style="padding:0px 15px 30px 15px;background-color: #E6E7E8">
				<div class="container">
					<div class="row featured-boxes">						
						<h3 align="center" style="color:#111;font-weight:bold;margin:80px 0px 0px;">Our Locations</h3>
						<p align="center" style="color:#D60B27;font-weight:bold;margin:10px 0px 20px;">Find and Contact Us</p>
						<!-- <div class="location-border"> -->
						
							

								<!-- 	<li style="list-style:none"><p style="font-size:1.2em;margin-bottom:5px;color:#111111;"><strong>Jakarta</strong></p></li>
									<li style="list-style:none"><p style="margin-bottom:0px;color:#111111;"><i class="icon icon-map-marker" style="color:#111111;margin:0px 7px 0px 0px"></i> Jl. Senopati Raya No. 27 - 29, Kebayoran Baru, Jakarta Selatan</p></li>
									<li style="list-style:none"><p style="color:#111111;"><i class="icon icon-phone" style="color:#111111;;margin:0px 7px 0px 0px"></i> +(6221) 5263354</p></li>
									
									<li style="list-style:none"><p style="font-size:1.2em;margin-bottom:5px;color:#111111;"><strong>Bali</strong></p></li>
									<li style="list-style:none"><p style="margin-bottom:0px;color:#111111;"><i class="icon icon-map-marker" style="color:#111111;margin:0px 7px 0px 0px"></i> Jl. Pura Mertasari, Sunset Road, Kuta Bali</p></li>
									<li style="list-style:none"><p style="color:#111111;"><i class="icon icon-phone" style="color:#111111;;margin:0px 7px 0px 0px"></i> +(62) 85857125005</p></li>
									
									<li style="list-style:none"><p style="font-size:1.2em;margin-bottom:5px;color:#111111;"><strong>Samarinda</strong></p></li>
									<li style="list-style:none"><p style="margin-bottom:0px;color:#111111;"><i class="icon icon-map-marker" style="color:#111111;margin:0px 7px 0px 0px"></i> Jl. Slamet Riady No. 6 RT / RW 014 Kel. Karang Asemkkir, Kec. Sungai Kunjang</p></li>
									<li style="list-style:none"><p style="color:#111111;"><i class="icon icon-phone" style="color:#111111;;margin:0px 7px 0px 0px"></i> - </p></li>
									
									<li style="list-style:none"><p style="font-size:1.2em;margin-bottom:5px;color:#111111;"><strong>Sukabumi</strong></p></li>
									<li style="list-style:none"><p style="margin-bottom:0px;color:#111111;"><i class="icon icon-map-marker" style="color:#111111;margin:0px 7px 0px 0px"></i> Jl. Surya Kencana No. 5 Sukabumi</p></li>
									<li style="list-style:none"><p style="color:#111111;"><i class="icon icon-phone" style="color:#111111;;margin:0px 7px 0px 0px"></i> +(62266) 6250968, +(62266) 6250802, +(62) 82320696969</p></li> -->
							

						<div class="row" style="margin-top: 15px;">
							<div class="col-md-12">
								<div class="lc-card">
								    <div class="column-lc">
										<div id="gmap" style="max-width:500px;height:416px;margin-left: 0px; margin-left: auto;margin-right: auto;"></div>
								    </div>
								    	
									<div class="lc-description">
								     	<div class="col-md-6" style="margin-top:5px">
											<div class="contact-details" style="margin-top: 10px;">
												<h3 style="color: #222;font-weight: bold;">Head Office</h3>
												<ul class="contact" align="left">
													<li style="list-style:none"><p style="font-size:20px;margin-bottom:10px;color:#AF0606;"><strong>Bandung</strong></p></li>
													<li style="list-style:none"><p style="font-size: 16px;;margin-bottom:3px;color:#111111;"><i class="icon icon-map-marker" style="color:#111111;margin:0px 7px 0px 0px"></i> JL. Babakan Jeruk III No.19</p></li>
													<li style="list-style:none"><p style="font-size: 16px;margin-bottom:3px;color:#111111;"><i class="icon icon-phone" style="color:#111111;margin:0px 7px 0px 0px"></i> +(6222) 2033335 </p></li>
													<li style="list-style:none"><p style="font-size: 16px;margin-bottom:3px;color:#111111;"><i class="icon icon-print" style="color:#111111;margin:0px 7px 0px 0px"></i> +(6222) 2033335 </p></li>
													<li style="list-style:none"><p style="font-size: 16px;color:#111111;"><i class="icon icon-envelope" style="color:#111111;margin:0px 7px 0px 0px"></i>info@zamedia.co.id</p></li>
												</ul>
											</div>
										</div>

										<div class="col-md-6" style="margin-top: 5px;">
											<div class="contact-details" style="margin-top: 10px;">
												<h3 style="color: #222;font-weight: bold;">Contact Us</h3>
												<form method="POST" action="">
													<input class="form-control" type="name" name="fname" placeholder="Enter Your Name"><br>
													<input class="form-control" type="email" name="fname" placeholder="Enter Your Email" style="margin-top: 15px"><br>
													<input class="form-control" type="subject" name="fname" placeholder="Enter Your Subject" style="margin-top: 15px"><br>
													<textarea class="form-control" type="message" name="fname" placeholder="Write Your Message" style="margin-top: 15px"></textarea>
													<button type="submit" name="send" class="send-button white-text" style="margin-top: 20px;">SEND</button>
												</form>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			


<!-- MODAL NEWS -->

			<!-- Modal -->
			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 align="center" class="modal-title" style="color: #D60B27; font-weight: bold;">Live Channel</h4>
						</div>
						<div class="modal-body">
							<div class="col-md-6">
								<div id="myCarousel3" class="carousel slide" data-ride="carousel">
									<!-- INDICATORS -->
									<ol class="carousel-indicators">
										<li data-target="#myCarousel3" data-slide-to="0" class="active"></li>
										<li data-target="#myCarousel3" data-slide-to="1"></li>
										<li data-target="#myCarousel3" data-slide-to="2"></li>
									</ol>
									<!-- Wrappers for Slides -->
									<div class="carousel-inner" role="listbox">
										<div class="item active">
											<img src="img/pa1.jpg" alt="pa1">
										</div>

										<div class="item">
											<img src="img/pa1.jpg" alt="pa2">
										</div>

										<div class="item">
											<img src="img/pa1.jpg" alt="pa3">
										</div>	
									</div>
									<!-- Kiri Kanan Kontrol -->
									<a class="left carousel-control" href="#myCarousel3" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="right carousel-control" href="#myCarousel3" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>
							</div>
							
							<h4 align="center" style="color: #D60B27;">Live Channel</h4>
							<p style="color: #000">Pop-up ads or pop-ups are forms of online advertising on the World Wide Web. A pop-up is a graphical user interface (GUI) display area, usually a small window, that suddenly appears ("pops up") in the foreground of the visual interface.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</div>
				</div>
			</div>



			<!-- Modal -->
			<div id="myModal2" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 align="center" class="modal-title" style="color: #D60B27; font-weight: bold;">Live Channel</h4>
						</div>
						<div class="modal-body">
							<div class="col-md-12">
								<center><img src="img/proser1.png" class="aikon"></center>
							</div>
							
							<h4 align="center" style="color: #D60B27;">Live Channel</h4>
							<p style="color: #000">Pop-up ads or pop-ups are forms of online advertising on the World Wide Web. A pop-up is a graphical user interface (GUI) display area, usually a small window, that suddenly appears ("pops up") in the foreground of the visual interface.</p>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>


				</div>
			</div>


			<div id="myModal3" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 align="center" class="modal-title" style="color: #D60B27; font-weight: bold;">Live News</h4>
						</div>
						<div class="modal-body">
							<div class="col-md-12">
								<center><img src="img/proser2.png" class="aikon"></center>
							</div>
							
							<h4 align="center" style="color: #D60B27;">Live News</h4>
							<p style="color: #000">Pop-up ads or pop-ups are forms of online advertising on the World Wide Web. A pop-up is a graphical user interface (GUI) display area, usually a small window, that suddenly appears ("pops up") in the foreground of the visual interface.</p>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>


			<div id="myModal4" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 align="center" class="modal-title" style="color: #D60B27; font-weight: bold;">Live News</h4>
						</div>
						<div class="modal-body">
							<div class="col-md-12">
								<center><img src="img/proser1.png" class="aikon"></center>
							</div>
							
							<h4 align="center" style="color: #D60B27;">Live News</h4>
							<p style="color: #000">Pop-up ads or pop-ups are forms of online advertising on the World Wide Web. A pop-up is a graphical user interface (GUI) display area, usually a small window, that suddenly appears ("pops up") in the foreground of the visual interface.</p>
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>



<!-- END MODAL NEWS -->



			<footer id="footer">
				<div class="container">
					<div class="row" style="padding-left:15px;padding-right:15px;padding-bottom:30px">
						<div class="col-md-4">
							<a href="index.php" class="logo push-bottom" style="text-decoration:none;font-family: 'Righteous', cursive;color:#fff;margin:10px 0 25px 0"><?php echo $lang_footer_title_about;  ?></a>
							<p align="justify"><?php echo $lang_footer_content_about;  ?></p>
						</div>
						<div class="col-md-4">
							<a href="index.php" class="logo push-bottom" style="text-decoration:none;font-family: 'Righteous', cursive;color:#fff;margin:10px 0 25px 0"><?php echo $lang_footer_navigation;  ?></a>
							<div class="contact-details">
								<ul class="contact">
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i><?php echo $lang_menu_proser;  ?></p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> <?php echo $lang_menu_video;  ?></p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> <?php echo $lang_menu_news;  ?></p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> <?php echo $lang_menu_location;  ?></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4">
							<a href="index.php" class="logo push-bottom" style="text-decoration:none;font-family: 'Righteous', cursive;color:#fff;margin:10px 0 25px 0"><?php echo $lang_footer_getupdates;  ?></a>
							<p align="justify">Get the new info and promo about FlexLive :</p>
							<div class="contact-details">								
								<ul class="contact">
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> Facebook</p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> Twitter</p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> Instagram</p></li>
									<li><p><i class="icon icon-caret-right" style="color:#efefef"></i> Youtube</p></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright" style="padding-left:15px;padding-right:15px">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="text-center">
								<a class="up-arrow" href="#mainMenu" data-toggle="tooltip" title="TO TOP">
									<span class="glyphicon glyphicon-chevron-up naik"></span>
								</a>
								</div>
								<p align="center" style="font-size:14px;margin-left:10px;margin-top:25px">Flex Live © 2019. All Rights Reserved.</p>
								<div align="center" style="margin:15px 0px 0px">
									<i class="icon icon-facebook-square" style="color:#ffffff;font-size:24px;margin:0 8px"></i>
									<i class="icon icon-twitter-square" style="color:#ffffff;font-size:24px;margin:0 8px"></i>
									<i class="icon icon-instagram" style="color:#ffffff;font-size:24px;margin:0 8px"></i>
									<i class="icon icon-youtube-play" style="color:#ffffff;font-size:24px;margin:0 8px"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>

		<script>
		function openNav() {
			document.getElementById("mySidenav").style.width = "250px";
		}

		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
		}

		
		</script>

		<!-- Libs -->
		<script src="vendor/jquery.js"></script>
		<script src="vendor/bootstrap.min.js"></script>
		<script src="vendor/masonry.js"></script>
		<script src="vendor/jquery.gmap.js"></script>
		<script src="js/theme.js"></script>
		
		<script type="text/javascript" src="js/jquery.ba-cond.min.js"></script>
		<script type="text/javascript" src="js/jquery.slitslider.js"></script>
		<script type="text/javascript" src="js/slitslider.js"></script>
		<script type="text/javascript" src="js/owl.carousel.min.js"></script>

		<script type="text/javascript">
			function naon() {
				$('#modal-form').modal('show');
				$('.modal-title').text('ini title');
			}
		</script>

		<script src="https://maps.google.com/maps/api/js?key=AIzaSyDg4Iu9EE5_Jn2hB_F-LQJsHIpJ9ghQgNE&sensor=false" type="text/javascript"></script>
		<!-- Placed at the end of the document so the pages load faster -->
		<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
		<!-- Responsive Video Slider JS -->
		<!-- <script src="assets/js/rvslider.min.js"></script> -->

		<!-- <script type="text/javascript">
			jQuery(function($){
			$(".rvs-container").rvslider();
		});
		</script> -->

		<!-- ANIMASI SMOOTH SCROLL -->
		<script>
			$(document).ready(function(){

			  // Add scrollspy to <body>
			  $('body').scrollspy({target: ".navbar", offset: 50});   

			  	// Initialize Tooltip
			  $('[data-toggle="tooltip"]').tooltip();

			  // Add smooth scrolling on all links inside the navbar
			  $("#mainMenu a").on('click', function(event) {
			    // Make sure this.hash has a value before overriding default behavior
			    if (this.hash !== "") {
			      // Prevent default anchor click behavior
			      event.preventDefault();

			      // Store hash
			      var hash = this.hash;

			      // Using jQuery's animate() method to add smooth page scroll
			      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			      $('html, body').animate({
			        scrollTop: $(hash).offset().top
			      }, 800, function(){
			   
			        // Add hash (#) to URL when done scrolling (default click behavior)
			        window.location.hash = hash;
			      });
			    }  // End if
			  });
			});
		</script>
		
		
		<script>
			/* Invisible Header */
			$(window).on("scroll", function() {
				if($(window).scrollTop() > 50 & $(window).width() > 768) {
					$("#header").addClass("bg-header");
				} else {
				    $("#header").removeClass("bg-header");
				}
			});
			
			/* Owl Carousel */
			$(document).ready(function() {
				$('.owl-carousel-tips').owlCarousel({
					items:1,
					merge:true,
					loop:true,
					margin:10,
					video:true,
					lazyLoad:true,
					center:true,
					responsive:{
						480:{
							items:2
						},
						600:{
							items:4
						}
					}
				})
				
				$('.owl-carousel-autowidth').owlCarousel({
					margin: 10,
					loop: true,
					autoWidth: true,
					responsive:{
						480:{
							items:2
						},
						600:{
							items:4
						}
					}
				})
				
				$('.owl-carousel-promo').owlCarousel({
					loop:true,
					margin:10,
					responsive:{
						0:{
							items:1
						},
						600:{
							items:1
						},
						1000:{
							items:1
						}
					}
				})
			})
		</script>
		
		<script type="text/javascript">
			var locations = [
			  ['Flex Live Bandung', -6.890148, 107.585056],
			  // ['Flex Live Jakarta', -6.2307336, 106.8064186, 5],
			  // ['Flex Live Sukabumi', -6.9188662, 106.9260259, 3],
			  // ['Flex Live Bali', -8.7079788, 115.1848375, 2],
			  // ['Flex Live Samarinda', -0.5131496, 117.1145556, 1]
			];

			var map = new google.maps.Map(document.getElementById('gmap'), {
			  zoom: 4,
			  center: new google.maps.LatLng(-3, 112),
			  mapTypeId: google.maps.MapTypeId.ROADMAP,
			  styles: [
			  {
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#212121"
				  }
				]
			  },
			  {
				"elementType": "labels.icon",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#212121"
				  }
				]
			  },
			  {
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "administrative.country",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#9e9e9e"
				  }
				]
			  },
			  {
				"featureType": "administrative.land_parcel",
				"stylers": [
				  {
					"visibility": "off"
				  }
				]
			  },
			  {
				"featureType": "administrative.locality",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#bdbdbd"
				  }
				]
			  },
			  {
				"featureType": "poi",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#181818"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "poi.park",
				"elementType": "labels.text.stroke",
				"stylers": [
				  {
					"color": "#1b1b1b"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "geometry.fill",
				"stylers": [
				  {
					"color": "#2c2c2c"
				  }
				]
			  },
			  {
				"featureType": "road",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#8a8a8a"
				  }
				]
			  },
			  {
				"featureType": "road.arterial",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#373737"
				  }
				]
			  },
			  {
				"featureType": "road.highway",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#3c3c3c"
				  }
				]
			  },
			  {
				"featureType": "road.highway.controlled_access",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#4e4e4e"
				  }
				]
			  },
			  {
				"featureType": "road.local",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#616161"
				  }
				]
			  },
			  {
				"featureType": "transit",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#757575"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "geometry",
				"stylers": [
				  {
					"color": "#000000"
				  }
				]
			  },
			  {
				"featureType": "water",
				"elementType": "labels.text.fill",
				"stylers": [
				  {
					"color": "#3d3d3d"
				  }
				]
			  }
			]
			});

			var infowindow = new google.maps.InfoWindow();

			var marker, i;

			for (i = 0; i < locations.length; i++) { 
			  marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i][1], locations[i][2]),
				map: map
			  });

			  google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
				  infowindow.setContent(locations[i][0]);
				  infowindow.open(map, marker);
				}
			  })(marker, i));
			}
		</script>
	</body>
</html>
